import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import * as firebase from 'firebase';

import { SigninPage } from '../pages/signin/signin';

const config = {
  apiKey: "AIzaSyC_vAdi-90iq9N448G3l7NPZCf7mStQu50",
  authDomain: "chatter-15b1b.firebaseapp.com",
  databaseURL: "https://chatter-15b1b.firebaseio.com",
  projectId: "chatter-15b1b",
  storageBucket: "chatter-15b1b.appspot.com",
  messagingSenderId: "1035686567"
};

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any = 'SigninPage';

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });

    firebase.initializeApp(config);
  }
}

